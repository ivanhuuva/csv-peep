## csv-peep
A command line tool written in bash using `column` and other command line tools to conveniently view csv-files in terminal. Uses `fzf` (or `dmenu`) to allow for interactive selection of fields to display.

### Usage

`csv-peep test_file.csv` brings up an `fzf` menu consisting of the field names, from which the desired fields are chosen using the tab key. Once the desired fields are selected, pressing enter outputs the resulting table. Using `dmenu` as the selector backend, fields are instead selected using ctrl+enter, and the final field using only enter, which also utputs the resulting table.

### Change the field selector backend
To use dmenu instead of `fzf`, comment out the `fzf`-lines in the choose_fields function, and uncomment the dmenu lines. 
